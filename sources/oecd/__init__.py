import sources.oecd.oda

name = "OECD"
description = "Module to access data from the OECD"
url = 'https://data.oecd.org/'